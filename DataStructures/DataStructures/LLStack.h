#pragma once
#include "LinkedList.h"
#include "LLNode.h"

class LLStack
{
public:
	LLStack();
	~LLStack();

	void	Push(int value);
	void	Pop();
	int		Peek();
	int		GetLength();
	void	PrintAll();

private:
	int count;
	LinkedList * LL;
};

