// DataStructures.cpp : Defines the entry point for the console application.
//

// Adding in a comment for testing purposes

#include "stdafx.h"
#include <iostream>
#include "LinkedList.h"
#include "LLStack.h"

// Function Signatures
void LLStackTest();

int main()
{
	//LinkedListTest();
	LLStackTest();

	system("Pause");
	return 0;
}

void LinkedListTest() {
	LinkedList * LL = new LinkedList();
	LL->AddNode(24);
	LL->AddNode(23);
	LL->AddNode(1);
	LL->AddNode(51);
	LL->AddNode(12);

	// Pre end add
	std::cout << "Pre-End Addition" << std::endl;
	LL->PrintNodes();

	// Post end add
	std::cout << "\nPost-End Addition" << std::endl;
	LL->AddNodeToEnd(99);
	LL->PrintNodes();

	// Removal Test
	std::cout << "\nRemove 51" << std::endl;
	LL->RemoveNode(51);
	LL->PrintNodes();
	std::cout << "\nRemove 4, nothing should happen" << std::endl;
	LL->RemoveNode(4);
	LL->PrintNodes();
	std::cout << "\nRemove 12, nothing should happen" << std::endl;
	LL->RemoveNode(12);
	LL->PrintNodes();

	// Add to location Test
	std::cout << "\nAdd 99 after the value 23" << std::endl;
	LL->AddNodeAfterValue(99, 23);
	LL->PrintNodes();

	std::cout << "\nRemove 99, nothing should happen" << std::endl;
	LL->RemoveNode(99);
	LL->PrintNodes();
	std::cout << "\nRemove 99, nothing should happen" << std::endl;
	LL->RemoveNode(99);
	LL->PrintNodes();

}

void LLStackTest() {
	LLStack * stack = new LLStack();
	
	// Test current stack
	std::cout << "Current Size: " << stack->GetLength() << std::endl;

	// Test insertion of 1000
	for (int i = 0; i < 1000; i++)
	{
		stack->Push(i);
	}

	// Test current stack size
	std::cout << "Current Size: " << stack->GetLength() << std::endl;
	//stack->PrintAll();

	// Test insertion of 10000
	for (int i = 0; i < 10000; i++)
	{
		stack->Push(i);
	}

	// Test current stack size
	std::cout << "Current Size: " << stack->GetLength() << std::endl;
	//stack->PrintAll();

	// Removal of 500
	for (int i = 0; i < 500; i++)
	{
		stack->Pop();
	}

	std::cout << "Current Size: " << stack->GetLength() << std::endl;

	// Removal of 12000
	for (size_t i = 0; i < 12000; i++)
	{
		std::cout << "removing value: " << stack->Peek() << std::endl;
		stack->Pop();
	}

	std::cout << "Current Size: " << stack->GetLength() << std::endl;

	delete stack;
}
